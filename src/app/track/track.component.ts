import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { CatServiceService } from 'src/app/cat-service.service';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  ogcat;
  stat;

  public counts = ["Order Placed","Shipping","Out For Delivery",
  "Delivered"];
  public orderStatus = ""
  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService) { 
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
        //this.getstat(user);
      })
      this.getcategories();
      this.orderStatus = this.os.item;
  }
  // getstat(user){
  //   return this.db.collection('Orders').doc(user.uid).collection('myOrders').doc(this.os.order.payload.doc.id).collection('cart').doc(this.os.item.payload.doc.id).snapshotChanges().subscribe(res => {this.stat = res; console.log(res); this.orderStatus = this.stat.Status});
  // }

  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
    
  }
}

