import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from 'src/app/cat-service.service';
import { OrderService } from '../order.service';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  items;
  orderss;
  ogcat;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService) { 
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
        this.getorders(user);
      })
      this.getcategories();
  }
  getorders(user){
    return this.db.collection('Orders').doc(user.uid).collection('myOrders').snapshotChanges().subscribe(res => {this.orderss = res; console.log(res); console.log(user.uid)});
  }

  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
  }

  logout() {
    this.auth.logout();
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
    
  }

  item(data){
    this.os.setorder(data);
    this.router.navigate(['/myordercart']);
  }
}
