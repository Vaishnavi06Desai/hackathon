import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  order;
  item;
  constructor() { }

  setorder(order)
  {
    this.order = order;
  }

  setitem(item)
  {
    this.item = item;
  }
}
