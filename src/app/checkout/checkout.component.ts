import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth/auth.service';
import { CatServiceService } from '../cat-service.service';
import { PaymentService } from '../payment.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ICustomWindow, WindowRefService } from '../window-ref.service';
import { exit } from 'process';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { appInjector } from '../injector';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  opened: boolean;
  user: firebase.User;
  price: number = 5000;
  ogcat;
  address;
  storeadd;
  mycart;
  addresses;
  paid = false;

  private _window: ICustomWindow;
  public rzp: any;
  email2: string;
  name2: string;
  amount2: number;
  phone2: number;
  userData: any;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private ps: PaymentService,
    private zone: NgZone,
    private winref: WindowRefService,) {
    this._window = this.winref.nativeWindow;
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
        this.getaddress();
        this.getproducts();
        this.getaddresses(user);
        this.email2 = user.email;
        this.name2 = user.displayName;
        this.options.prefill.name = this.name2;
        this.options.prefill.email = this.email2;
        //this.amount2 = user.amount;

        console.log(this.price * 100);
        this.getUserData(this.user.uid);

      })
    this.getcategories();
    this.price = this.ps.price;
    this.options.amount = (this.price) * 100;
  }


  public options: any = {
    key: 'rzp_test_aWpcTLtlgMcZHH', //add razorpay key here
    name: 'The Hyphens',
    description: 'Shopping',

    amount: this.price, //razorpay takes amount in paise
    prefill: {
      name: this.name2,
      //email: this.email2 //Add your email here
      email: this.email2,
      contact: this.phone2
    },
    notes: {},
    rs: this.router,
    theme: {
      color: '#3330FF'
    },
    'handler': function (response) {
    
      //alert(response.razorpay_payment_id);
      if (!(typeof response.razorpay_payment_id == 'undefined' || response.razorpay_payment_id < 1)) {
        window.alert("Success!");
      }
    },

  };

  initPay(): void {
    if (this.ps.address != "") {
      this.paid = true;
      this.rzp = new this.winref.nativeWindow['Razorpay'](this.options);
      //this.rzp = new this.winref.nativeWindow.Razorpay(this.options);
      this.rzp.open();
    }
    else{
      window.alert('Please submit address');
    }
  }

  paymentHandler(res: any) {
    this.zone.run(() => {
      //add API call here
    });
  }

  getUserData(uid) {

    return this.db.collection('Users').doc(uid).snapshotChanges().subscribe(res => {
      this.userData = res.payload.data(); console.log(res); this.options.prefill.contact = this.userData.phnum;

    });
  }

  form1 = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form1.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form1.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  form = new FormGroup({
    Address: new FormControl(''),
  })

  exitpay() {
    if (this.paid){
    this.db.collection('Orders').doc(this.user.uid).collection('myOrders').doc(Date()).set({Name: Date()})
    for (let i = 0; i < this.mycart.length; i++) {
      this.db.collection('Orders').doc(this.user.uid).collection('myOrders').doc(Date()).collection('cart').add({ ShippingAddress: this.address.address, TotalPayment: this.price, Price: this.mycart[i].payload.doc.data().Price, Category: this.mycart[i].payload.doc.data().Category, Item: this.mycart[i].payload.doc.data().Item, Name: this.mycart[i].payload.doc.data().Name, Url: this.mycart[i].payload.doc.data().Url, Status: "Order Placed" });
      this.db.collection('Products').doc(this.mycart[i].payload.doc.data().Item).collection('myUsers').add({ id: this.user.uid })
    }
    this.router.navigate(['/orders']);
    //return this.db.collection('Cart').doc(this.user.uid).collection('itens').snapshotChanges().subscribe(res => {res.map(a => {this.db.collection('Cart').doc(this.user.uid).collection('items').doc(a.payload.doc.id).delete()})})
    // this.db.collection('Cart').doc(this.user.uid).delete();
  }
    else{
      window.alert("Please complete payment");
    }
  }
    
  onSubmit() {
    this.storeadd = this.form.get('Address').value;
    this.form.get('Address').setValue('');
    this.ps.setadd(this.storeadd);
    for(let i = 0; i < (this.addresses).length; i++)
    {
      if(this.storeadd == this.addresses[i].payload.doc.data().address)
      {
        return;
      }
    }
    this.db.collection('UserAddresses').doc(this.user.uid).collection("Addresses").add({address: this.storeadd});
  }
  getcategories() {
    return this.db.collection('Categories').snapshotChanges().subscribe(res => { this.ogcat = res; });
  }

  getaddress() {
    return this.db.collection('Users').doc(this.user.uid).snapshotChanges().subscribe(res => { this.address = res.payload.data(); this.form.get('Address').setValue(this.address.address); });
  }

  getproducts() {
    return this.db.collection('Cart').doc(this.user.uid).collection('items').snapshotChanges().subscribe(res => { this.mycart = res; console.log(this.mycart[0].payload.doc.data()) });
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string) {
    this.cs.getcategory(category);
    this.router.navigate(['/category']);

  }

  getaddresses(user)
  {
    return this.db.collection('UserAddresses').doc(user.uid).collection('Addresses').snapshotChanges().subscribe(res => {this.addresses = res; console.log(res)});
  }

  fill(data)
  {
    this.form.get('Address').setValue(data.payload.doc.data().address);
  }
}
