import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatServiceService {
  
  category: string;
  item:string;
  search;

  constructor() { }

  getcategory(category){
    this.category = category;
    console.log(this.category);
  }

  getitem(item)
  {
    this.item = item;
    console.log(this.item);
  }

  setsearch(search)
  {
    this.search = search;
  }
}
