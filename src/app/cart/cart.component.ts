import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { HostListener } from '@angular/core';
import {Location} from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth/auth.service';
import { CatServiceService } from '../cat-service.service';
import { map, switchMap } from 'rxjs/operators';
import { PaymentService } from '../payment.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  items;
  ogcat;
  sum = 0;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private ps: PaymentService) { 
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
        this.getcart(user.uid);
      })
      this.getcategories();
  }

  getcart(curuser)
  {
    return this.db.collection('Cart').doc(curuser).collection('items').snapshotChanges().subscribe(res => {this.items = res; this.sum = 0; res.map(a => {this.sum = this.sum + a.payload.doc.data().Price}); this.ps.setprice(this.sum);});
  }

  delete(data)
  {
    return this.db.collection('Cart').doc(this.user.uid).collection('items').doc(data.payload.doc.id).delete();
  }

  gotoshop(data)
  {
    this.cs.getcategory(data.payload.doc.data().Category);
    this.cs.getitem(data.payload.doc.data().Item);
    this.router.navigate(['/category/item']);
  }

  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
    
  }

  
  back()
  {
    window.history.back();
  }

  checkout()
  {
    this.router.navigate(['/checkout']);
  }
}
