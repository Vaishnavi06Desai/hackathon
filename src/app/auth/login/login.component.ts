import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authError: any;
  user:any;

  constructor(private auth: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.auth.eventAuthError$.subscribe( data => {
      this.authError = data;
    });
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
      if(user)
      {
        this.nav();
      }
    })
  }

  nav()
  {
    this.router.navigate(['/home']);
  }

  login(frm) {
    this.auth.login(frm.value);
  }

}
