import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { CatServiceService } from '../cat-service.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ResourceLoader } from '@angular/compiler';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  cat;
  name;
  ogcat

  constructor(private auth: AuthService,
    private db: AngularFirestore,
    private router: Router,
    private cs: CatServiceService) { }

    ngOnInit(): void {
      this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
      })
      this.getcatelauge();
      this.getcategories();
      this.getname();
    }

    getcatelauge(){
      return this.db.collection('Categories').doc(this.cs.category).collection('Inventory').snapshotChanges().subscribe(res => {this.cat = res; console.log(res)});
    }

    getcategories(){
      return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
    }

    logout() {
      this.auth.logout();
    }
  
    currcat(category: string){
      this.cs.getcategory(category);
      this.router.navigate(['/ccategory']);
      
    }

    getname(){
      // return this.db.collection('Categories').doc(this.cs.category).snapshotChanges().subscribe(res => {this.name = res; console.log(res); console.log("Hi")});
      return this.db.collection('Categories').doc(this.cs.category).snapshotChanges().subscribe(res => {this.name = res; console.log(res)});
    }

    form = new FormGroup({
      Search: new FormControl(''),
    })
    search()
    {
      if(this.form.get('Search').value == "")
      {
        window.alert("Enter a valid search.")
      }
      else{
        this.cs.setsearch(this.form.get('Search').value)
        this.router.navigate(['/results'])
      }
    }

    setitem(item: string){
      this.cs.getitem(item);
      console.log(item);
      this.router.navigate(['/category/item']);
    }

}
