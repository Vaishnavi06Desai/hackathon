import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { CatServiceService } from '../cat-service.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  opened: boolean;
  user: firebase.User;
  item;
  feedback;
  email = "";
  err: number = 0;
  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: number;
  data;
  ogcat;
  nstar = 0;
  iscustom = false;
  customs;

  constructor(private auth: AuthService,
    private db: AngularFirestore,
    private router: Router,
    private cs: CatServiceService) { }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
        this.email = user.email;
        this.getcustomers(user);
      })
    this.getitem();
    console.log(this.cs.category)
    console.log(this.cs.item, "HII")
    this.getfeedback();
    this.getcategories();
  }

  form = new FormGroup({
    Comments: new FormControl(''),
    Name: new FormControl(this.email),
    options: new FormControl('')
  })

  form1 = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form1.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form1.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  countStar(star) {
    this.selectedValue = star;
    this.nstar = star
    console.log('Value of star', star);
  }

  getcustomers(user){
    return this.db.collection('Products').doc(this.cs.item).collection('myUsers').snapshotChanges().subscribe(res => {this.customs = res; res.map(a => {if(a.payload.doc.data().id == user.uid){this.iscustom = true} })});
  }

  onSubmitStar()
  {
    //console.log(sum, noc, this.data.Sum)
    if (Math.floor((this.data.Sum + this.nstar)/(this.data.NoOfCustomers + 1)) >= 4)
    {
      this.db.collection('BestSellers').add({Url: this.data.Url, Name: this.data.Name, ProductID: this.cs.item, Category: this.cs.category})
    }
    return this.db.collection('Categories').doc(this.cs.category).collection('Inventory').doc(this.cs.item).update({Sum: this.data.Sum + this.nstar, NoOfCustomers: this.data.NoOfCustomers + 1, CustomerFeedback: Math.floor((this.data.Sum + this.nstar)/(this.data.NoOfCustomers + 1))});
  }

  updateFeedback(data) {
    return new Promise<any>((resolve, reject) => { this.db.collection('Categories').doc(this.cs.category).collection('Inventory').doc(this.cs.item).collection('Feedback').add(data).then(() => { resolve("Success!!") }, err => reject(err)); });
  }

  delete(data) {
    return this.db.collection('Categories').doc(this.cs.category).collection('Inventory').doc(this.cs.item).collection('Feedback').doc(data.payload.doc.id).delete();
  }

  subscribe(){
    return new Promise<any>((resolve, reject) => {this.db.collection('Subscriptions').doc(this.user.uid).collection("mySubscriptions").add({name: this.item.payload.data().Seller}).then(() => {resolve(window.alert("Subscibed!!"))}, err => reject(err));});
    
  }

  onSubmit() {
    this.form.get('Name').setValue(this.email);
    if (this.form.get('Comments').value == ''){
      this.form.get('Comments').setValue(this.form.get('options').value);
    }
    let data = this.form.value;
    console.log(this.email);
    this.updateFeedback(data).then(res => { console.log(res); this.clear(); }, err => { console.log(err); this.err = 1 });
  }

  getitem() {
    //return this.db.collection('Categories').doc(this.cs.category).collection('Inventory').doc(this.cs.item).snapshotChanges().subscribe(res => {this.item = res; console.log(res.payload.data()); console.log("Hi");});
    return this.db.collection('Categories').doc(this.cs.category).collection('Inventory').doc(this.cs.item).snapshotChanges().subscribe(res => { this.item = res; this.data = res.payload.data(); console.log(res); console.log("Hi"); });
  }

  getfeedback() {
    return this.db.collection('Categories').doc(this.cs.category).collection('Inventory').doc(this.cs.item).collection('Feedback').snapshotChanges().subscribe(res => { this.feedback = res; console.log(res); console.log("Hi"); });
  }

  clear()
  {
    this.form.get('Comments').setValue('');
  }

  del(data)
  {
    this.delete(data);
  }

  edit(data)
  {
    this.form.get('Comments').setValue(data.payload.doc.data().Comments);
    this.delete(data);
  }

  shop()
  {
    this.db.collection('Cart').doc(this.user.uid).collection('items').add({Category: this.cs.category, Item: this.cs.item, Url: this.data.Url, Price: this.data.Price, Name: this.data.Name});
    this.router.navigate(['/cart']);
  }

  logout() {
    this.auth.logout();
  }

  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
  }


  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
    
  }
}
