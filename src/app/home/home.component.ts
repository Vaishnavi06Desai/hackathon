import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { HostListener } from '@angular/core';
import {Location} from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth/auth.service';
import { CatServiceService } from '../cat-service.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  slideIndex: number;
  opened: boolean;  
  user: firebase.User;
  count: number = 0;
  images:any;
  cat;
  bs;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService) { 
    this.slideIndex = 0;
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
      })
      this.getimages();
      this.getcategories();
      this.getbestseller();
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.cat = res; console.log(res)});
  }

  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  gotoshop(data)
  {
    this.cs.getcategory(data.payload.doc.data().Category);
    this.cs.getitem(data.payload.doc.data().ProductID);
    console.log(data.payload.doc.data().Category, data.payload.doc.data().ProductID);
    this.router.navigate(['/category/item']);
  }

  getbestseller()
  {
    return this.db.collection('BestSellers').snapshotChanges().subscribe(res => {this.bs = res})
  }

  getimages()
  {
      return this.db.collection('images').snapshotChanges().subscribe(res => {this.images = res; console.log(res)});
  }

  showSlides() {
    var i;
    let slides = <HTMLElement[]><any>document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    this.slideIndex++;
    if (this.slideIndex > slides.length) {this.slideIndex = 1}
    slides[this.slideIndex-1].style.display = "block";
    setTimeout(() => {this.showSlides()}, 2000);

  }

  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
  }

  logout() {
    this.auth.logout();
  }

}
