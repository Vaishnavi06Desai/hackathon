import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from '../cat-service.service';
import { OrderService } from '../order.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  opened: boolean;
  user: firebase.User;
  ogcat;
  stat;
  notifications;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService) {
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
        this.getnot(user);
      })
    this.getcategories();
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  getnot(user)
  {
    return this.db.collection('Notifications').doc(user.uid).collection('MyNotifications').snapshotChanges().subscribe(res => {this.notifications = res; console.log(res)});
  }

  getcategories() {
    return this.db.collection('Categories').snapshotChanges().subscribe(res => { this.ogcat = res; console.log(res) });
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string) {
    this.cs.getcategory(category);
    this.router.navigate(['/category']);

  }

  delete(data)
  {
    return this.db.collection('Notifications').doc(this.user.uid).collection('MyNotifications').doc(data.payload.doc.id).delete();
  }

  gotoshop(data)
  {
    this.cs.getcategory(data.payload.doc.data().Category);
    this.cs.getitem(data.payload.doc.data().ProductID);
    console.log(data.payload.doc.data().Category, data.payload.doc.data().ProductID);
    this.router.navigate(['/category/item']);
  }

}
