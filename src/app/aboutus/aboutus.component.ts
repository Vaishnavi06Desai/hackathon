import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from '../cat-service.service';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  ogcat;
  stat;

  constructor(private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService) { }

  ngOnInit(): void {

    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
        //this.getstat(user);
      })
      this.getcategories();
      //this.orderStatus = this.os.item;
  }

  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
    
  }


}
