import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from '../cat-service.service';
import { OrderService } from '../order.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

  opened: boolean;
  user: firebase.User;
  ogcat;
  stat;
  sellers;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService) {
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
        this.getsellers(user);
      })
    this.getcategories();
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  getsellers(user)
  {
    return this.db.collection('Subscriptions').doc(user.uid).collection('mySubscriptions').snapshotChanges().subscribe(res => {this.sellers = res});
  }

  getcategories() {
    return this.db.collection('Categories').snapshotChanges().subscribe(res => { this.ogcat = res; console.log(res) });
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string) {
    this.cs.getcategory(category);
    this.router.navigate(['/category']);

  }
}

