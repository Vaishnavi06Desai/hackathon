import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from 'src/app/cat-service.service';
import { OrderService } from '../order.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-carts',
  templateUrl: './carts.component.html',
  styleUrls: ['./carts.component.scss']
})
export class CartsComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  items;
  ogcat;
  stat;
  public counts = ["Order Placed","Shipping","Out For Delivery",
  "Delivered"];
  public orderStatus = ""

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService) { 
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }
  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
        this.getitems(user);
      })
      this.getcategories();
      //this.orderStatus = this.os.item;
  }
  getitems(user){
    return this.db.collection('Orders').doc(user.uid).collection('myOrders').doc(this.os.order.payload.doc.id).collection('cart').snapshotChanges().subscribe(res => {this.items = res; console.log(res)});
  }

  getcategories(){
    return this.db.collection('Categories').snapshotChanges().subscribe(res => {this.ogcat = res; console.log(res)});
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string){
    this.cs.getcategory(category);
    this.router.navigate(['/category']);
    
  }

  track(data){
    this.os.setitem(data.payload.doc.data().Status);
    this.router.navigate(['/track']);
  }

  gotoshop(data)
  {
    this.cs.getcategory(data.payload.doc.data().Category);
    this.cs.getitem(data.payload.doc.data().Item);
    this.router.navigate(['/category/item']);
  }
}



