import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopysearchComponent } from './copysearch.component';

describe('CopysearchComponent', () => {
  let component: CopysearchComponent;
  let fixture: ComponentFixture<CopysearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopysearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopysearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
