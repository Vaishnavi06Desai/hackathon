import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from '../cat-service.service';
import { OrderService } from '../order.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  opened: boolean;
  user: firebase.User;
  ogcat;
  stat;
  sellers;
  mysearch;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService) {
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
      })
    this.getcategories();
    this.getitems();
    console.log(this.cs.search)

  }

  gotoshop(data)
  {
    this.cs.getcategory(data.payload.doc.data().Category);
    this.cs.getitem(data.payload.doc.data().ProductID);
    this.router.navigate(['/category/item']);
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })

  getitems()
  {
    return this.db.collection('Tags').doc(this.cs.search).collection('Items').snapshotChanges().subscribe(res => {this.mysearch = res});
  }
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/search'])
    }
  }


  getcategories() {
    return this.db.collection('Categories').snapshotChanges().subscribe(res => { this.ogcat = res; console.log(res) });
  }

  logout() {
    this.auth.logout();
  }

  currcat(category: string) {
    this.cs.getcategory(category);
    this.router.navigate(['/category']);

  }

}
