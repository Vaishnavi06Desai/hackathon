import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CatServiceService } from '../cat-service.service';
import { OrderService } from '../order.service';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  opened: boolean;
  user: firebase.User;
  ogcat;
  stat;
  userProfile;

  constructor(
    private auth: AuthService,
    private router: Router,
    private db: AngularFirestore,
    private cs: CatServiceService,
    private os: OrderService,) {
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
        this.getUserProfile(user);
        //this.getstat(user);
      })
    this.getcategories();
  }
  // getstat(user){
  //   return this.db.collection('Orders').doc(user.uid).collection('myOrders').doc(this.os.order.payload.doc.id).collection('cart').doc(this.os.item.payload.doc.id).snapshotChanges().subscribe(res => {this.stat = res; console.log(res); this.orderStatus = this.stat.Status});
  // }
  getUserProfile(user){
    return this.db.collection('Users').doc(user.uid).snapshotChanges().subscribe(res=> {
     this.userProfile = res;
   });
  }

  getcategories() {
    return this.db.collection('Categories').snapshotChanges().subscribe(res => { this.ogcat = res; console.log(res) });
  }

  logout() {
    this.auth.logout();
  }

  form = new FormGroup({
    Search: new FormControl(''),
  })
  search()
  {
    if(this.form.get('Search').value == "")
    {
      window.alert("Enter a valid search.")
    }
    else{
      this.cs.setsearch(this.form.get('Search').value)
      this.router.navigate(['/results'])
    }
  }

  currcat(category: string) {
    this.cs.getcategory(category);
    this.router.navigate(['/category']);

  }
}
