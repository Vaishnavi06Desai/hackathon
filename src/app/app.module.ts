import { BrowserModule, HammerModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import * as Hammer from 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { HomeComponent } from './home/home.component';
import { CategoriesComponent } from './categories/categories.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';
import {MaterialsModule} from './materials/materials.module';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { ItemComponent } from './item/item.component';
import { CartComponent } from './cart/cart.component';
import { ProfileComponent } from './profile/profile.component';
import { CopycatComponent } from './copycat/copycat.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrdersComponent } from './orders/orders.component';
import { CartsComponent } from './carts/carts.component';
import { TrackComponent } from './track/track.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SearchComponent } from './search/search.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { CopysearchComponent } from './copysearch/copysearch.component';
;

@Injectable({providedIn: 'root'})
export class HammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    let mc = new Hammer(element, {
      touchAction: "pan-y",
    });
    return mc;
  }
} 


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CategoriesComponent,
    LoginComponent,
    RegistrationComponent,
    ItemComponent,
    CartComponent,
    ProfileComponent,
    CopycatComponent,
    CheckoutComponent,
    OrdersComponent,
    CartsComponent,
    TrackComponent,
    SubscribeComponent,
    NotificationsComponent,
    SearchComponent,
    AboutusComponent,
    CopysearchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

    NgbModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MaterialsModule,
    HammerModule,

    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'Pharmacad'),
    AngularFirestoreModule, // Only required for database features
    AngularFireAuthModule, // Only required for auth features,
    AngularFireStorageModule,

    FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [     
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }}, 
     {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
