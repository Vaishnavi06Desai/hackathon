import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { CategoriesComponent } from './categories/categories.component';
import { ItemComponent } from './item/item.component';
import { CartComponent } from './cart/cart.component';
import { ProfileComponent } from './profile/profile.component';
import { CopycatComponent } from './copycat/copycat.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrdersComponent } from './orders/orders.component';
import { CartsComponent } from './carts/carts.component';
import { TrackComponent } from './track/track.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SearchComponent } from './search/search.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { CopysearchComponent } from './copysearch/copysearch.component';


const routes: Routes = [
  {
    path:'home',
    component: HomeComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'register',
    component: RegistrationComponent
  },
  {
    path:'category',
    component: CategoriesComponent
  },
  {
    path:'category/item',
    component: ItemComponent
  },
  {
    path:'cart',
    component: CartComponent
  },
  {
    path:'profile',
    component: ProfileComponent
  },
  {
    path:'orders',
    component: OrdersComponent
  },
  {
    path:'ccategory',
    component: CopycatComponent
  },
  {
    path:'checkout',
    component: CheckoutComponent
  },
  {
    path:'orders',
    component: OrdersComponent
  },
  {
    path:'myordercart',
    component: CartsComponent
  },
  {
    path:'track',
    component: TrackComponent
  },
  {
    path:'sub',
    component: SubscribeComponent
  },
  {
    path:'not',
    component: NotificationsComponent
  },
  {
    path:'results',
    component: SearchComponent
  },
  {
    path:'aboutus',
    component: AboutusComponent
  },
  {
    path:'search',
    component: CopysearchComponent
  },
  {
    path:'',
    redirectTo:'/login',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
