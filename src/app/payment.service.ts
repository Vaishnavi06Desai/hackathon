import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  price = 0;
  address = "";
  constructor() { }

  setprice(price){
    this.price = price;
  }

  setadd(add){
    this.address = add;
  }

}
