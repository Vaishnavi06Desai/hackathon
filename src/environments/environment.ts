// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAPnq8XAkJ8zFzofAqrvGuIlq0JwheoLds",
    authDomain: "tsechackathon-b8fb7.firebaseapp.com",
    databaseURL: "https://tsechackathon-b8fb7.firebaseio.com",
    projectId: "tsechackathon-b8fb7",
    storageBucket: "tsechackathon-b8fb7.appspot.com",
    messagingSenderId: "801610341773",
    appId: "1:801610341773:web:38fbbd05af6f1d8cfa71c4",
    measurementId: "G-Q5ELKYN0MR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
